<?php

/**
 * @file context_theme_vars_reaction_vars_set.inc
 * Adds theme vars reaction to Context.
 */

/**
 * PHP code as a Context reaction.
 */
class context_theme_vars_reaction_vars_set extends context_reaction {
  /**
   * Editor form.
   */
  function editor_form($context) {
    $form['#type'] = 'value';
    $form['#value'] = TRUE;
    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  function editor_form_submit($context, $values) {
    return array($values);
  }

  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    return array(
      'var_set' => array(
        '#type' => 'textarea',
        '#title' => t('Vars to hide'),
        '#description' => t('Enter each vars that you need to hide. Separate by comma or new line. You can use format like page][sidebar_first.'),
        '#default_value' => isset($defaults['var_set']) ? $defaults['var_set'] : '',
      ),
    );
  }

  function execute(&$vars) {
    foreach (context_active_contexts() as $context) {
      $options = $this->fetch_from_context($context, 'options');
      if (!empty($options['var_set'])) {
        $var_set = $options['var_set'];
        $var_set = array_filter(array_map("trim", explode("\n", str_replace(",", "\n", $var_set))));
        foreach ($var_set as $k) {
          $k = explode('][', $k);
          $k = "['" . implode("']['", $k) . "']";
          eval("if (is_array(\$vars{$k})) {\$vars{$k} = array();}");
        }
      }
    }
  }
}
